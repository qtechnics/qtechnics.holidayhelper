﻿using System;

namespace QTechnics.HolidayHelper.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            HolidayHelper.AddIslamicHolidays();
            HolidayHelper.AddTurkeyRepublicHolidays();
            HolidayHelper.AddWeekendHolidays();
            Console.WriteLine(HolidayHelper.CountWeekdayBetweenDates(new DateTime(2019, 7, 1), new DateTime(2019, 7, 31), DayOfWeek.Wednesday));
            HolidayHelper.IsHolidayWithDetails(new DateTime(2019, 1, 1), new DateTime(2019, 12, 31))
                .ForEach(f =>
                {
                    Console.WriteLine($"Date : {f.Date:D} \tIsHoliday : {f.IsHoliday} \tIsCalendarHoliday : {f.IsCalendarHoliday} \tIsDayOfWeekHoliday : {f.IsDayOfWeekHoliday}");
                    f.Holidays.ForEach(ff =>
                    {
                        Console.WriteLine($"\tDescription : {ff.Description} \tIsHijri : {ff.IsHijri}");
                    });
                });
        }
    }
}
