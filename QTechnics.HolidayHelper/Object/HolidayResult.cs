﻿using System;
using System.Collections.Generic;

namespace QTechnics.HolidayHelper
{
    /// <summary>
    /// Result of holiday
    /// </summary>
    public class HolidayResult
    {
        /// <summary>
        /// Holiday date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// A Collection of holidays of date
        /// </summary>
        public List<Holiday> Holidays { get; set; } = new List<Holiday>();

        /// <summary>
        /// true if the date is holiday, otherwise false
        /// </summary>
        public bool IsHoliday => Holidays.Count > 0 || IsDayOfWeekHoliday;

        /// <summary>
        /// true if the date is calender holiday, otherwise false
        /// </summary>
        public bool IsCalendarHoliday { get; set; }

        /// <summary>
        /// true if the date is day of week holiday, otherwise false
        /// </summary>
        public bool IsDayOfWeekHoliday { get; set; }

        /// <summary>
        /// true if more than one holiday, otherwise false
        /// </summary>
        public bool IsMultipleHolidays => Holidays.Count > 1;
    }
}
