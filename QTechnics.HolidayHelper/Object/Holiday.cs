﻿namespace QTechnics.HolidayHelper
{
    /// <summary>
    /// Holiday
    /// </summary>
    public class Holiday
    {
        /// <summary>
        /// Day of month
        /// </summary>
        public int Day { get; set; }

        /// <summary>
        /// Month of year
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// true if holiday is hijri calendar, otherwise false
        /// </summary>
        public bool IsHijri { get; set; }

        /// <summary>
        /// Description of holiday
        /// </summary>
        public string Description { get; set; }
    }
}
