﻿using System;

namespace QTechnics.HolidayHelper
{
    /// <summary>
    /// Day of week Holiday
    /// </summary>
    public class DayOfWeekHoliday
    {
        /// <summary>
        /// Day of week
        /// </summary>
        public DayOfWeek DayOfWeek { get; set; }

        /// <summary>
        /// true if the day of week is holiday, otherwise false
        /// </summary>
        public bool IsHoliday { get; set; }
    }
}
