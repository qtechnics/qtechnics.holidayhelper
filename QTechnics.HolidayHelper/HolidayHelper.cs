﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace QTechnics.HolidayHelper
{
    /// <summary>
    /// Gregorian and Hijri Holiday Helper Library
    /// </summary>
    public static class HolidayHelper
    {
        #region properties

        /// <summary>
        /// Holiday list
        /// </summary>
        public static List<Holiday> Holidays { get; } = new List<Holiday>();

        /// <summary>
        /// Holidays of the week
        /// </summary>
        public static List<DayOfWeekHoliday> DayOfWeekHoliday { get; } = new List<DayOfWeekHoliday>()
        {
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Monday },
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Tuesday },
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Wednesday },
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Thursday },
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Friday },
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Saturday },
            new DayOfWeekHoliday() { DayOfWeek = DayOfWeek.Sunday }
        };

        #endregion

        #region methods

        /// <summary>
        /// Checks whether the given date is a holiday
        /// </summary>
        /// <param name="date">Date to check</param>
        /// <returns>true if the date is holiday, otherwise false</returns>
        public static bool IsHoliday(DateTime date) => _checkHoliday(date).IsHoliday;

        /// <summary>
        /// Checks whether the given date is a holiday
        /// </summary>
        /// <param name="date">Date to check</param>
        /// <returns>HolidayResult object if the date is holiday, otherwise null</returns>
        public static HolidayResult IsHolidayWithDetails(DateTime date) => _checkHoliday(date);

        /// <summary>
        /// Checks whether the given between date1 and date2 is a holiday
        /// </summary>
        /// <param name="date1">First date to check</param>
        /// <param name="date2">Last date to check</param>
        /// <returns>true if the any date in range is holiday, otherwise false</returns>
        public static bool IsHoliday(DateTime date1, DateTime date2)
        {
            date1 = date1.Date;
            date2 = date2.Date;

            bool ret = false;

            if (date1 > date2) throw new ArgumentOutOfRangeException();

            int dayCounter = (date2 - date1).Days + 1;

            for (int i = 0; i < dayCounter; i++)
                if (_checkHoliday(date1.AddDays(i)).IsHoliday) ret = true;

            return ret;
        }

        /// <summary>
        /// Checks whether the given between date1 and date2 is a holiday
        /// </summary>
        /// <param name="date1">First date to check</param>
        /// <param name="date2">Last date to check</param>
        /// <returns>HolidayResult object collection if the any date in range is holiday, otherwise null</returns>
        public static List<HolidayResult> IsHolidayWithDetails(DateTime date1, DateTime date2)
        {
            date1 = date1.Date;
            date2 = date2.Date;

            List<HolidayResult> ret = new List<HolidayResult>();

            if (date1 > date2) throw new ArgumentOutOfRangeException();

            int dayCounter = (date2 - date1).Days + 1;

            for (int i = 0; i < dayCounter; i++)
            {
                var holiday = _checkHoliday(date1.AddDays(i));
                if (holiday.IsHoliday)
                    ret.Add(holiday);
            }

            return ret;
        }

        /// <summary>
        /// Add custom holiday
        /// </summary>
        /// <param name="holiday">Holiday object</param>
        public static void AddHoliday(Holiday holiday)
        {
            if (Holidays.Any(a => a.Day == holiday.Day && a.Month == holiday.Month && a.IsHijri == holiday.IsHijri))
                throw new DuplicateWaitObjectException();

            Holidays.Add(holiday);
        }

        /// <summary>
        /// Remove custom holiday
        /// </summary>
        /// <param name="holiday">Holiday object</param>
        public static void RemoveHoliday(Holiday holiday)
        {
            var item = Holidays.Where(a => a.Day == holiday.Day && a.Month == holiday.Month && a.IsHijri == holiday.IsHijri).FirstOrDefault();

            if (item == null) throw new KeyNotFoundException();

            Holidays.Remove(item);
        }

        /// <summary>
        /// Clear all holidays
        /// </summary>
        public static void ClearHolidays() => Holidays.Clear();

        /// <summary>
        /// Add holiday of day of week
        /// </summary>
        /// <param name="dayOfWeek">Day of Week</param>
        public static void AddDayOfWeekHoliday(DayOfWeek dayOfWeek) => DayOfWeekHoliday.FirstOrDefault(f => f.DayOfWeek == dayOfWeek).IsHoliday = true;

        /// <summary>
        /// Remove holiday of day of week
        /// </summary>
        /// <param name="dayOfWeek">Day of Week</param>
        public static void RemoveDayOfWeekHoliday(DayOfWeek dayOfWeek) => DayOfWeekHoliday.FirstOrDefault(f => f.DayOfWeek == dayOfWeek).IsHoliday = false;

        /// <summary>
        /// Reset holiday of day of week
        /// </summary>
        public static void ResetWeekdayHoliday() => DayOfWeekHoliday.ForEach(f => f.IsHoliday = false);

        /// <summary>
        /// Adds Islamic holidays (Ramadan & Sacrifice Feast)
        /// </summary>
        /// <param name="IsIncludeEve">Include eve (arafe)</param>
        public static void AddIslamicHolidays(bool IsIncludeEve = true)
        {

            for (int i = 0; i < 3; i++)
            {
                var islamicHoliday = new Holiday() { Day = i + 1, Month = 10, IsHijri = true, Description = $"Ramazan Bayramı {i + 1}.Gün" };
                Holidays.Add(islamicHoliday);
            }

            for (int i = 0; i < 4; i++)
            {
                var islamicHoliday = new Holiday() { Day = i + 10, Month = 12, IsHijri = true, Description = $"Kurban Bayramı {i + 1}.Gün" };
                Holidays.Add(islamicHoliday);
            }

            if (IsIncludeEve)
            {
                Holidays.Add(new Holiday() { Day = 30, Month = 9, IsHijri = true, Description = $"Ramazan Bayramı Arefesi" });
                Holidays.Add(new Holiday() { Day = 9, Month = 12, IsHijri = true, Description = $"Kurban Bayramı Arefesi" });
            }
        }

        /// <summary>
        /// Adds the official holiday of the Republic of Turkey
        /// </summary>
        /// <param name="CumhuriyetBayramiBirBucukGun">Include eve of national republic holiday</param>
        public static void AddTurkeyRepublicHolidays(bool CumhuriyetBayramiBirBucukGun = true)
        {
            Holidays.Add(new Holiday() { Day = 1, Month = 1, IsHijri = false, Description = "Yılbaşı" });
            Holidays.Add(new Holiday() { Day = 23, Month = 4, IsHijri = false, Description = "23 Nisan Ulusal Egemenlik ve Çocuk Bayramı" });
            Holidays.Add(new Holiday() { Day = 1, Month = 5, IsHijri = false, Description = "1 Mayıs Emek ve Dayanışma Günü" });
            Holidays.Add(new Holiday() { Day = 19, Month = 5, IsHijri = false, Description = "19 Mayıs Atatürk'ü Anma Gençlik ve Spor Bayramı" });
            Holidays.Add(new Holiday() { Day = 15, Month = 7, IsHijri = false, Description = "15 Temmuz Demokrasi ve Milli Birlik Günü" });
            Holidays.Add(new Holiday() { Day = 30, Month = 8, IsHijri = false, Description = "30 Ağustos Zafer Bayramı" });
            if (CumhuriyetBayramiBirBucukGun)
                Holidays.Add(new Holiday() { Day = 28, Month = 10, IsHijri = false, Description = "29 Ekim Cumhuriyet Bayramı" });
            Holidays.Add(new Holiday() { Day = 29, Month = 10, IsHijri = false, Description = "29 Ekim Cumhuriyet Bayramı" });
        }

        /// <summary>
        /// Adds default gregorian day of week holidays
        /// </summary>
        /// <param name="IsStaurday">Include saturday</param>
        public static void AddWeekendHolidays(bool IsStaurday = true) => DayOfWeekHoliday.Where(w => w.DayOfWeek == DayOfWeek.Sunday || (IsStaurday && w.DayOfWeek == DayOfWeek.Saturday)).ToList().ForEach(f => f.IsHoliday = true);

        /// <summary>
        /// Count whether the given between date1 and date2 is a day of week
        /// </summary>
        /// <param name="date1">First date to check</param>
        /// <param name="date2">Last date to check</param>
        /// <param name="dayOfWeek">Day of week</param>
        /// <returns></returns>
        public static int CountWeekdayBetweenDates(DateTime date1, DateTime date2, DayOfWeek dayOfWeek)
        {
            date1 = date1.Date;
            date2 = date2.Date;

            int ret = 0;

            if (date1 > date2) throw new ArgumentOutOfRangeException();

            int dayCounter = (date2 - date1).Days + 1;

            for (int i = 0; i < dayCounter; i++)
                if (date1.AddDays(i).DayOfWeek == dayOfWeek) ret++;

            return ret;
        }

        #endregion

        private static HolidayResult _checkHoliday(DateTime date)
        {
            int day = date.Day;
            int month = date.Month;
            int year = date.Year;

            HijriCalendar hijriCalendar = new HijriCalendar();

            int hijriDay = hijriCalendar.GetDayOfMonth(date);
            int hijriMonth = hijriCalendar.GetMonth(date);
            int hijrYear = hijriCalendar.GetYear(date);

            HolidayResult result = new HolidayResult() { Date = date };

            if (DayOfWeekHoliday.FirstOrDefault(f => f.DayOfWeek == date.DayOfWeek).IsHoliday)
            {
                result.IsDayOfWeekHoliday = true;
            }

            var gregorianHoliday = Holidays.Where(w => !w.IsHijri && w.Day == day && w.Month == month).FirstOrDefault();

            if (gregorianHoliday != null)
            {
                result.Holidays.Add(gregorianHoliday);
                result.IsCalendarHoliday = true;
            }

            var hijriHoliday = Holidays.Where(w => w.IsHijri && w.Day == hijriDay && w.Month == hijriMonth).FirstOrDefault();

            if (hijriHoliday != null)
            {
                result.Holidays.Add(hijriHoliday);
                result.IsCalendarHoliday = true;
            }

            return result;
        }

    }
}
